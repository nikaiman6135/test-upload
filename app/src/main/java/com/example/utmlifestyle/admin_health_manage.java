package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class admin_health_manage extends AppCompatActivity {

    private FirebaseRecyclerOptions<HRecordSearch> display;
    private FirebaseRecyclerAdapter<HRecordSearch, MyViewHolder2> adapter;

    private RecyclerView recyclerView;

    DatabaseReference ref;

    EditText edittext, ETDate;

    Button search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_health_manage);

        ref = FirebaseDatabase.getInstance().getReference().child("Health_Record");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ETDate = findViewById(R.id.date);
        search = findViewById(R.id.search);

        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.date);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "yyyy-MM-dd"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                ETDate.setText(sdf.format(myCalendar.getTime()));

                String Sdate = ETDate.getText().toString().trim();

                search.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(ETDate.getText())){
                            Toast.makeText(getApplicationContext(), "Please enter a date!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), Sdate, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), admin_health_manage2.class);
                            intent.putExtra("date", Sdate);
                            startActivity(intent);
                        }
                    }
                });
            }
        };
        edittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(admin_health_manage.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker


        display = new FirebaseRecyclerOptions.Builder<HRecordSearch>().setQuery(ref, HRecordSearch.class).build();
        adapter = new FirebaseRecyclerAdapter<HRecordSearch, MyViewHolder2>(display) {
            @Override
            protected void onBindViewHolder(@NonNull MyViewHolder2 holder, int position, @NonNull HRecordSearch model) {
                holder.userID.setText("" + model.getId());
                holder.date.setText("" + model.getDate());
                holder.medication.setText("" + model.getMedication());
                holder.reason.setText("" + model.getReason());
                holder.note.setText("" + model.getNote());

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), admin_health_manage3.class);
                        intent.putExtra("userID", model.getId());
                        intent.putExtra("date", model.getDate());
                        intent.putExtra("medication", model.getMedication());
                        intent.putExtra("reason", model.getReason());
                        intent.putExtra("note", model.getNote());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_record1, parent, false);
                return new MyViewHolder2(v);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}