package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class venue_search extends AppCompatActivity {

    TextView date;
    String gDate;

    RecyclerView recyclerView_booking_list;
    Booking_List_Adapter2 booking_list_adapter;
    List<Venue_booking_statuss> booking_listList = new ArrayList<>();
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_search);

        recyclerView_booking_list = findViewById(R.id.recyclerview_booking_list);
        recyclerView_booking_list.setHasFixedSize(true);
        recyclerView_booking_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        Intent i = getIntent();
        gDate = i.getStringExtra("date");
        date = findViewById(R.id.date);
        date.setText(gDate);

        //get id number
        String userID;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Users");
        final TextView id = findViewById(R.id.id);
        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);
                if (userProfile != null){
                    String ids = userProfile.id;
                    id.setText(ids);

                    //recyclerview
                    reference  = FirebaseDatabase.getInstance().getReference("Venue_booking_statuss");
                    Query query = reference.orderByChild("rent_person_name").equalTo(ids);
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            booking_listList.clear();
                            for(DataSnapshot snapshot1 : snapshot.getChildren()){
                                Venue_booking_statuss venue_booking_statuss = snapshot1.getValue(Venue_booking_statuss.class);
                                if(venue_booking_statuss.getRent_status().equals("n")&&venue_booking_statuss.getDate().equals(gDate)){
                                    booking_listList.add(venue_booking_statuss);
                                }
                            }
                            booking_list_adapter = new Booking_List_Adapter2(getApplicationContext(), booking_listList);
                            booking_list_adapter.setOnItemClickListener(new Booking_List_Adapter2.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(getApplicationContext(), venue_manage.class);
                                    intent.putExtra("date", booking_listList.get(position).getDate());
                                    intent.putExtra("time", booking_listList.get(position).getTime());
                                    intent.putExtra("venue", booking_listList.get(position).getName());
                                    intent.putExtra("payment", booking_listList.get(position).getPay_method());
                                    intent.putExtra("id", booking_listList.get(position).getRent_person_name());
                                    intent.putExtra("bookingID", booking_listList.get(position).getRent_id());
                                    startActivity(intent);
                                }
                            });
                            recyclerView_booking_list.setAdapter(booking_list_adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                    //end of recyclerview
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(), "Something wrong happened!", Toast.LENGTH_LONG).show();
            }
        });
        //end of get id number




    }
}