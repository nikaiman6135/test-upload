package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class venue_confirm extends AppCompatActivity {

    String gVenue, gDate, gTime, gID;

    TextView venue, date, time, bookingID;

    Button backButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_confirm);

        Intent i = getIntent();
        gVenue = i.getStringExtra("venue");
        gID = i.getStringExtra("bookingID");
        gDate = i.getStringExtra("date");
        gTime = i.getStringExtra("time");

        venue = findViewById(R.id.venue);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        bookingID = findViewById(R.id.bookingID);

        venue.setText(gVenue);
        date.setText(gDate);
        time.setText(gTime);
        bookingID.setText(gID);

        //get user name
        String userID;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Users");
        final TextView name = findViewById(R.id.name);
        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);

                if (userProfile != null){
                    String names = userProfile.name;
                    name.setText(names);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(venue_confirm.this, "Something wrong happened!", Toast.LENGTH_LONG).show();

            }
        });

        backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}