package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class venue_cancel extends AppCompatActivity {

    TextView date, time, bookingID, venue, payment;

    Button delete, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_cancel);

        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        bookingID = findViewById(R.id.bookingID);
        venue = findViewById(R.id.venue);
        payment = findViewById(R.id.payment);

        String adate = getIntent().getStringExtra("date");
        String atime = getIntent().getStringExtra("time");
        String abookingID = getIntent().getStringExtra("bookingID");
        String aVenue = getIntent().getStringExtra("venue");
        String aPayment = getIntent().getStringExtra("payment");


        date.setText(adate);
        time.setText(atime);
        bookingID.setText(abookingID);
        venue.setText(aVenue);
        payment.setText(aPayment);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        delete = findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(aVenue+";"+adate+";"+l2s(atime)).child("pay_method").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(aVenue+";"+adate+";"+l2s(atime)).child("rent_id").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(aVenue+";"+adate+";"+l2s(atime)).child("rent_person_name").setValue("");
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(aVenue+";"+adate+";"+l2s(atime)).child("rent_status").setValue("a");

                Intent intent = new Intent(venue_cancel.this, venue_cancel2.class);
                venue_cancel.this.startActivity(intent);
                finish();

            }
        });
    }

    String l2s(String time){
        String[] sc = time.split("");
        String sn = "";
        for(int i = 0; i<2;i++){
            sn +=sc[i];
        }
        //Toast.makeText(TrackingMapActivity.this, sn, Toast.LENGTH_SHORT).show();
        return sn;
    }
}