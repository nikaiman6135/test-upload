package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class admin_advertisement_manage extends AppCompatActivity {

    private FirebaseRecyclerOptions<AdvertisementSearch> display;
    private FirebaseRecyclerAdapter<AdvertisementSearch,MyViewHolder> adapter;

    private RecyclerView recyclerView;

    DatabaseReference ref;

    ImageView adPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_advertisement_manage);

        ref = FirebaseDatabase.getInstance().getReference().child("Advertisement");

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adPicture = findViewById(R.id.adPicture);

        display = new FirebaseRecyclerOptions.Builder<AdvertisementSearch>().setQuery(ref, AdvertisementSearch.class).build();
        adapter = new FirebaseRecyclerAdapter<AdvertisementSearch, MyViewHolder>(display) {
            @Override
            protected void onBindViewHolder(@NonNull MyViewHolder holder, int position, @NonNull AdvertisementSearch model) {

                final String date = model.getDate();
                final String time = model.getTime();
                final String title = model.getTitle();
                final String description = model.getDescription();
                final String url = model.getUrl();

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), admin_advertisement_manage2.class);
                        intent.putExtra("date", date);
                        intent.putExtra("time", time);
                        intent.putExtra("title", title);
                        intent.putExtra("description", description);
                        intent.putExtra("url", url);
                        startActivity(intent);
                    }
                });

                holder.textTitle.setText(""+model.getTitle());
                holder.textDate.setText(""+model.getDate());
                holder.textTime.setText(""+model.getTime());
                holder.textDescription.setText(""+model.getDescription());
                Picasso.get().load(model.getUrl()).into(holder.adPicture);
            }

            @NonNull
            @Override
            public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_advertisement_single,parent,false);
                return new MyViewHolder(v);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}