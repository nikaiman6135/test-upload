package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class clinic_cancel extends AppCompatActivity {

    TextView date, time, apptID;

    Button delete, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_cancel);

        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        apptID = findViewById(R.id.apptID);

        String adate = getIntent().getStringExtra("date");
        String atime = getIntent().getStringExtra("time");
        String aapptID = getIntent().getStringExtra("apptID");

        date.setText(adate);
        time.setText(atime);
        apptID.setText(aapptID);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        delete = findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("Clinic").orderByChild("apptID").equalTo(aapptID);

                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot snapshot1: snapshot.getChildren()){

                            snapshot1.getRef().removeValue();

                            Intent intent = new Intent(clinic_cancel.this, clinic_cancel2.class);
                            clinic_cancel.this.startActivity(intent);
                            finish();

                            //Toast.makeText(getApplicationContext(), aapptID, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(), "Connection to database failed.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}