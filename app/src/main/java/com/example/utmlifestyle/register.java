package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Pattern;

public class register extends AppCompatActivity implements View.OnClickListener {

    private FirebaseAuth mAuth;

    private Button register;
    private EditText ETname, ETid, ETdob, ETemail, ETphone, ETaddress, ETpassword, ETconfirmPassword;

    private ProgressBar progressBar;

    private TextView textLogin;

    EditText etet;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();

        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(this);

        textLogin = findViewById(R.id.textLogin);
        textLogin.setOnClickListener(this);

        ETname = findViewById(R.id.name);
        ETid = findViewById(R.id.id);
        ETdob = findViewById(R.id.dob);
        ETemail = findViewById(R.id.email);
        ETphone = findViewById(R.id.phone);
        ETaddress = findViewById(R.id.address);
        ETpassword = findViewById(R.id.password);
        ETconfirmPassword = findViewById(R.id.confirmPassword);

        progressBar = findViewById(R.id.progressBar);

        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        etet = (EditText) findViewById(R.id.dob);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
            private void updateLabel() {
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                etet.setText(sdf.format(myCalendar.getTime()));
                ETdob.setText(sdf.format(myCalendar.getTime()));
            }
        };
        etet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(register.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.register:
                registerUser();
                break;

            case R.id.textLogin:
                Intent i = new Intent(getApplicationContext(), login.class);
                startActivity(i);
                finish();
                break;
        }
    }

    private void registerUser() {

        String name = ETname.getText().toString().trim();
        String id = ETid.getText().toString().trim();
        String dob = ETdob.getText().toString().trim();
        String email = ETemail.getText().toString().trim();
        String phone = ETphone.getText().toString().trim();
        String address = ETaddress.getText().toString().trim();
        String password = ETpassword.getText().toString().trim();
        String confirmPassword = ETconfirmPassword.getText().toString().trim();

        if (name.isEmpty()){
            ETname.setError("Required!");
            ETname.requestFocus();
            return;
        }

        if (id.isEmpty()){
            ETid.setError("Required!");
            ETid.requestFocus();
            return;
        }

        if (dob.isEmpty()){
            ETdob.setError("Required!");
            ETdob.requestFocus();
            return;
        }

        /*if (!ValidDate(ETdob.getText().toString())){
            ETdob.setError("Invalid Date Or Wrong Format \n Please Use DD/MM/YYYY!");
            ETdob.requestFocus();
            return;
        }

         */

        if (email.isEmpty()){
            ETemail.setError("Required!");
            ETemail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            ETemail.setError("Invalid Email!");
            ETemail.requestFocus();
            return;
        }

        if (phone.isEmpty()){
            ETphone.setError("Required!");
            ETphone.requestFocus();
            return;
        }

        if (!Patterns.PHONE.matcher(phone).matches()){
            ETphone.setError("Invalid Phone Number!");
            ETphone.requestFocus();
            return;
        }

        if (address.isEmpty()){
            ETaddress.setError("Required!");
            ETaddress.requestFocus();
            return;
        }

        if (password.isEmpty()){
            ETpassword.setError("Required!");
            ETpassword.requestFocus();
            return;
        }

        if (password.length() < 6){
            ETpassword.setError("Password should be more than 6 characters");
        }

        if (confirmPassword.isEmpty()){
            ETconfirmPassword.setError("Required!");
            ETconfirmPassword.requestFocus();
            return;
        }

        if (!confirmPassword.equals(password)){
            ETconfirmPassword.setError("Password do not match!");
            ETconfirmPassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()){
                            User user = new User(name, id, dob, phone, email, address);

                            FirebaseDatabase.getInstance().getReference("Users")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isComplete()){

                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(register.this, "Register Successful", Toast.LENGTH_LONG).show();

                                        Intent i = new Intent(getApplicationContext(), register2.class);
                                        startActivity(i);
                                        finish();
                                    }else {

                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(register.this, "An account with similar email has been created. Please try with another email.", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }else {

                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(register.this, "An account with similar email has been created. Please try with another email.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
}