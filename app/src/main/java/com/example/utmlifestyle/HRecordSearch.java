package com.example.utmlifestyle;

public class HRecordSearch {

    String date, id, medication, note, reason;

    public HRecordSearch() {
    }

    public HRecordSearch(String date, String id, String medication, String note, String reason) {
        this.date = date;
        this.id = id;
        this.medication = medication;
        this.note = note;
        this.reason = reason;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
