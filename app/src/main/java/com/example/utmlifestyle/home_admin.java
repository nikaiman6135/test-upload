package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class home_admin extends AppCompatActivity {

    Button venue, rental, clinic, health, advertisement, profile, logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_admin);

        venue = findViewById(R.id.Venue);
        rental = findViewById(R.id.Rental);
        clinic = findViewById(R.id.Clinic);
        health = findViewById(R.id.HealthRecord);
        advertisement = findViewById(R.id.Advertisement);
        profile = findViewById(R.id.Profile);
        logout = findViewById(R.id.Logout);

        venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getApplicationContext(), health_record_display.class));
                startActivity(new Intent(getApplicationContext(), admin_venue.class));
            }
        });

        rental.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getApplicationContext(), health_record_display.class));
                Toast.makeText(getApplicationContext(), "Manage Equipment Rental", Toast.LENGTH_SHORT).show();
            }
        });

        clinic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(getApplicationContext(), health_record_display.class));
                Toast.makeText(getApplicationContext(), "Manage Clinic Appointment", Toast.LENGTH_SHORT).show();
            }
        });

        health.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), admin_health.class));
            }
        });

        advertisement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), admin_advertisement.class));
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), profile.class));
            }
        });

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
                finish();
                Toast.makeText(getApplicationContext(), "You have been sign out.", Toast.LENGTH_LONG).show();
            }
        });


    }
}