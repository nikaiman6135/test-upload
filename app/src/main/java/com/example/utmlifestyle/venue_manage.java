package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class venue_manage extends AppCompatActivity {

    TextView date, time, bookingID, venue, payment;
    Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_manage);

        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        bookingID = findViewById(R.id.bookingID);
        venue = findViewById(R.id.venue);
        payment = findViewById(R.id.payment);

        String adate = getIntent().getStringExtra("date");
        String atime = getIntent().getStringExtra("time");
        String abookingID = getIntent().getStringExtra("bookingID");
        String aVenue = getIntent().getStringExtra("venue");
        String aPayment = getIntent().getStringExtra("payment");

        date.setText(adate);
        time.setText(atime);
        bookingID.setText(abookingID);
        venue.setText(aVenue);
        payment.setText(aPayment);

        cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), venue_cancel.class);
                intent.putExtra("date", adate);
                intent.putExtra("time", atime);
                intent.putExtra("venue", aVenue);
                intent.putExtra("payment", aPayment);
                intent.putExtra("bookingID", abookingID);
                startActivity(intent);
                finish();
            }
        });
    }
}