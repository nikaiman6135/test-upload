package com.example.utmlifestyle;

public class User {

    public String name, id, dob, phone, email, address;

    public User() {

    }

    public User(String name, String id, String dob, String phone, String email, String address) {
        this.name = name;
        this.id = id;
        this.dob = dob;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }
}
