package com.example.utmlifestyle;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder2 extends RecyclerView.ViewHolder {

    TextView userID, date, medication, note, reason;
    View view;

    public MyViewHolder2(@NonNull View itemView) {
        super(itemView);

        userID = itemView.findViewById(R.id.userID);
        date = itemView.findViewById(R.id.date);
        medication = itemView.findViewById(R.id.medication);
        note = itemView.findViewById(R.id.note);
        reason = itemView.findViewById(R.id.reason);

        view = itemView;
    }
}
