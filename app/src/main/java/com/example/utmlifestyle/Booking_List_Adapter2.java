package com.example.utmlifestyle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Booking_List_Adapter2 extends RecyclerView.Adapter<Booking_List_Adapter2.ViewHolder> {

    Context context;
    List<Venue_booking_statuss> venue_listList;
    private OnItemClickListener mOnItemClickListener;

    public Booking_List_Adapter2(Context context, List<Venue_booking_statuss> venue_listList){
        this.context = context;
        this.venue_listList = venue_listList;
    }

    @NonNull
    @Override
    public Booking_List_Adapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_booking_list_adapter2, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Booking_List_Adapter2.ViewHolder holder, final int position) {
        holder.textview_bookingID.setText(venue_listList.get(position).getRent_id());
        holder.textview_venue.setText(venue_listList.get(position).getName());
        holder.textview_date.setText(venue_listList.get(position).getDate());
        holder.textview_time.setText(venue_listList.get(position).getTime());
        holder.textview_payment.setText(venue_listList.get(position).getPay_method());
        if (mOnItemClickListener != null) {
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onItemClick(view, position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return this.venue_listList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textview_date, textview_time, textview_bookingID, textview_venue, textview_payment;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textview_date = itemView.findViewById(R.id.date);
            textview_time = itemView.findViewById(R.id.time);
            textview_bookingID = itemView.findViewById(R.id.bookingID);
            textview_venue = itemView.findViewById(R.id.venue);
            textview_payment = itemView.findViewById(R.id.payment);
            imageView = itemView.findViewById(R.id.imageView7);
        }
    }

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener){
        this.mOnItemClickListener = mOnItemClickListener;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}
