package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class home_sports extends AppCompatActivity {

    Button venue, equipment, bmi, fitness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_sports);

        venue = findViewById(R.id.venue);
        equipment = findViewById(R.id.equipment);
        bmi = findViewById(R.id.bmi);
        fitness = findViewById(R.id.fitness);

        venue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home_sports.this, venue_home.class);
                startActivity(intent);
            }
        });

        equipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(home_sports.this, "equipment", Toast.LENGTH_SHORT).show();
            }
        });

        bmi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(home_sports.this, "bmi", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(home_sports.this, BMIActivity.class);
                startActivity(intent);
            }
        });

        fitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(home_sports.this, "fitness", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(home_sports.this, TrackingActivity.class);
                startActivity(intent);
            }
        });
    }
}