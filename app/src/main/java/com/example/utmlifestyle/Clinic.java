package com.example.utmlifestyle;

public class Clinic {
    private String id, etChooseTime, date, reason, apptID;

    public Clinic() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEtChooseTime() {
        return etChooseTime;
    }

    public void setEtChooseTime(String etChooseTime) {
        this.etChooseTime = etChooseTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getApptID() {
        return apptID;
    }

    public void setApptID(String apptID) {
        this.apptID = apptID;
    }
}
