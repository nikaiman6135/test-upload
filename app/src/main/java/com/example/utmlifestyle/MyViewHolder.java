package com.example.utmlifestyle;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    TextView textTitle, textDate, textTime, textDescription;

    ImageView adPicture;

    View view;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        textTitle = itemView.findViewById(R.id.textTitle);
        textDate = itemView.findViewById(R.id.textDate);
        textTime = itemView.findViewById(R.id.textTime);
        textDescription = itemView.findViewById(R.id.textDescription);

        adPicture = itemView.findViewById(R.id.adPicture);

        view = itemView;

    }
}
