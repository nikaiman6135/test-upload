package com.example.utmlifestyle;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class SearchVenueAdapter extends RecyclerView.Adapter<SearchVenueAdapter.ViewHolder> {

    Context context;
    List<VenueSearch> venueSearchList = new ArrayList<>();

    SearchVenueAdapter(Context context, List<VenueSearch> venueSearchList){
        this.context = context;
        this.venueSearchList = venueSearchList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_venue_single2, parent, false);
        return new SearchVenueAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.textview_date.setText(this.venueSearchList.get(position).getDate());
        holder.textview_time.setText(this.venueSearchList.get(position).getTime());
        holder.textview_bookingID.setText(this.venueSearchList.get(position).getBookingID());

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();

                final String date = venueSearchList.get(position).getDate();
                final String time  = venueSearchList.get(position).getTime();
                final String bookingID  = venueSearchList.get(position).getBookingID();

                Intent intent = new Intent(context, venue_manage.class);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                intent.putExtra("bookingID", bookingID);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.venueSearchList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textview_date, textview_time, textview_bookingID;

        public ViewHolder(@NonNull View itemView) {

            super(itemView);
            textview_date = itemView.findViewById(R.id.textDate);
            textview_time = itemView.findViewById(R.id.textTime);
            textview_bookingID = itemView.findViewById(R.id.bookingID);
            imageView = itemView.findViewById(R.id.imageView8);
        }
    }
}
