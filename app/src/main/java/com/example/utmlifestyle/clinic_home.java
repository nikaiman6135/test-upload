package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class clinic_home extends AppCompatActivity {

    private Button newAppointment, searchDateBtn;

    Calendar calendar;

    EditText mdate;
    EditText edittext;

    private RecyclerView recyclerView;

    private FirebaseUser user;
    private DatabaseReference reference;
    private Query query;

    private String userID;

    private FirebaseRecyclerOptions<ClinicSearch> display;
    private FirebaseRecyclerAdapter<ClinicSearch,ClinicView> adapter;

    EditText userID2;
    String userIDt;

    TextView history;

    String gDate;
    TextView date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_home);

        mdate = findViewById(R.id.date);
        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.date);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                mdate.setText(sdf.format(myCalendar.getTime()));

                gDate = mdate.getText().toString().trim();
            }
        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(clinic_home.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userID = user.getUid();

        userID2 = findViewById(R.id.userID2);

        reference.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User user = snapshot.getValue(User.class);

                if (user != null){
                    String idd = user.id;
                    userID2.setText(idd);

                    userIDt = userID2.getText().toString();

                    //Toast.makeText(clinic_home.this, userIDt, Toast.LENGTH_LONG).show();

                    query = FirebaseDatabase.getInstance().getReference().child("Clinic").orderByChild("id").equalTo(userIDt).limitToLast(3);

                    display = new FirebaseRecyclerOptions.Builder<ClinicSearch>().setQuery(query, ClinicSearch.class).build();
                    adapter = new FirebaseRecyclerAdapter<ClinicSearch, ClinicView>(display) {
                        @Override
                        protected void onBindViewHolder(@NonNull ClinicView holder, int position, @NonNull ClinicSearch model) {

                            final String date = model.getDate();
                            final String time = model.getEtChooseTime();
                            final String reason = model.getReason();
                            final String id = model.getId();
                            final String apptID = model.getApptID();

                            holder.view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(getApplicationContext(), clinic_manage.class);
                                    intent.putExtra("date", date);
                                    intent.putExtra("time", time);
                                    intent.putExtra("reason", reason);
                                    intent.putExtra("id", id);
                                    intent.putExtra("apptID", apptID);
                                    startActivity(intent);
                                }
                            });

                            holder.textDate.setText(""+model.getDate());
                            holder.textTime.setText(""+model.getEtChooseTime());

                        }

                        @NonNull
                        @Override
                        public ClinicView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_clinic_single, parent, false);
                            return new ClinicView(v);
                        }
                    };

                    adapter.startListening();
                    recyclerView.setAdapter(adapter);

                    //end
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(clinic_home.this, "Connection to database failed :(", Toast.LENGTH_LONG).show();
            }
        });
        //end of recycle view


        newAppointment = findViewById(R.id.newAppointment);
        searchDateBtn = findViewById(R.id.searchDateBtn);

        newAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), clinic_appointment.class);
                startActivity(i);
            }
        });

        //String gDate = mdate.getText().toString().trim();
        searchDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(mdate.getText())){
                    Toast.makeText(clinic_home.this, "Please enter a date!", Toast.LENGTH_LONG).show();
                }else {

                    //Toast.makeText(clinic_home.this, gDate, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), clinic_search.class);
                    intent.putExtra("date", gDate);
                    startActivity(intent);
                }
            }
        });

        history = findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), clinic_history.class);
                startActivity(i);
            }
        });
    }
}