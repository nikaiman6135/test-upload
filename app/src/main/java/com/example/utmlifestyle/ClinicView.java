package com.example.utmlifestyle;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ClinicView  extends RecyclerView.ViewHolder {

    TextView textDate, textTime, apptID;
    View view;

    public ClinicView(@NonNull View itemView) {

        super(itemView);

        textDate = itemView.findViewById(R.id.textDate);
        textTime = itemView.findViewById(R.id.textTime);
        apptID = itemView.findViewById(R.id.apptID);

        view = itemView;
    }
}
