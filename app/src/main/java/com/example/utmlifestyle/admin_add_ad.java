package com.example.utmlifestyle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.OnProgressListener;
import com.squareup.picasso.Picasso;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

public class admin_add_ad extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private EditText ETType, ETTitle, ETDate, ETTime,  ETDescription;

    private Button submitButton, chooseImage;

    private ImageView image_view;

    private static final int PICK_IMAGE_REQUEST = 1;

    private Uri ImageUri;

    private ProgressBar progressBar;

    private StorageReference StorageRef;
    private DatabaseReference DatabaseRef;

    EditText edittext;
    EditText chooseTime;
    TimePickerDialog timePickerDialog;
    Calendar calendar;
    int currentHour;
    int currentMinute;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_ad);

        ETType = findViewById(R.id.adType);
        ETTitle = findViewById(R.id.adTitle);
        ETDate = findViewById(R.id.adDate);
        ETTime = findViewById(R.id.adTime);
        ETDescription = findViewById(R.id.adDescription);

        submitButton = findViewById(R.id.submitButton);
        chooseImage = findViewById(R.id.chooseImage);

        image_view = findViewById(R.id.image_view);

        progressBar = findViewById(R.id.progressBar);

        StorageRef = FirebaseStorage.getInstance().getReference("Advertisement").child("images");
        DatabaseRef = FirebaseDatabase.getInstance().getReference("Advertisement");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadAd();
            }
        });

        chooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        //date picker
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.adDate);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "dd/MM/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                ETDate.setText(sdf.format(myCalendar.getTime()));
            }
        };

        edittext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(admin_add_ad.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker

        //time picker
        chooseTime = findViewById(R.id.adTime);
        chooseTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                timePickerDialog = new TimePickerDialog(admin_add_ad.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {

                        chooseTime.setText(String.format("%02d:%02d", hourOfDay, minutes));
                        ETTime.setText(String.format("%02d:%02d", hourOfDay, minutes));
                    }
                }, currentHour, currentMinute, false);

                timePickerDialog.show();
            }
        });
        //end of time picker

        //Advertisement Type
        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.adType, R.layout.custom_spinner1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        //end of Advertisement Type
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = parent.getItemAtPosition(position).toString();
        ETType.setText(text);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void openFileChooser(){
        Intent intent  = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            ImageUri = data.getData();
            Picasso.get().load(ImageUri).into(image_view);
        }
    }

    private String getFileExtension (Uri uri){
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadAd(){

        if (TextUtils.isEmpty(ETType.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETType.setError( "Dont Leave Me Empty :(" );

        }else if (TextUtils.isEmpty(ETTitle.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETTitle.setError( "Dont Leave Me Empty :(" );

        }else if (TextUtils.isEmpty(ETDate.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETDate.setError( "Dont Leave Me Empty :(" );

        }else if (TextUtils.isEmpty(ETTime.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETTime.setError( "Dont Leave Me Empty :(" );

        }else if (TextUtils.isEmpty(ETDescription.getText())){
            Toast.makeText(admin_add_ad.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
            ETDescription.setError( "Dont Leave Me Empty :(" );

        }else {

            if (ImageUri != null){

                progressBar.setVisibility(View.VISIBLE);

                StorageReference fileReference = StorageRef.child(System.currentTimeMillis()
                        + "." + getFileExtension(ImageUri));

                fileReference.putFile(ImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>()
                {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception
                    {
                        if (!task.isSuccessful())
                        {
                            throw task.getException();
                        }
                        return fileReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task)
                    {
                        if (task.isSuccessful())
                        {
                            Uri downloadUri = task.getResult();

                            UploadAd upload = new UploadAd(ETType.getText().toString().trim(),
                                    ETTitle.getText().toString().trim(),
                                    ETDate.getText().toString().trim(),
                                    ETTime.getText().toString().trim(),
                                    ETDescription.getText().toString().trim(),
                                    downloadUri.toString());

                            String uploadTitle = ETTitle.getText().toString().trim();

                            DatabaseRef.orderByChild("title").equalTo(uploadTitle).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot snapshot) {
                                    if (snapshot.exists()){
                                        Toast.makeText(getApplicationContext(), "There exist an advertisement with same name!", Toast.LENGTH_LONG).show();
                                    } else{
                                        DatabaseRef.child(uploadTitle).setValue(upload);
                                        progressBar.setVisibility(View.GONE);
                                        Toast.makeText(admin_add_ad.this, "Upload Success", Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError error) {
                                    Toast.makeText(admin_add_ad.this, "Cant connect to Database", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else{

                            Toast.makeText(admin_add_ad.this, "Upload Failed", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }else {
                Toast.makeText(this, "Image not selected", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

