package com.example.utmlifestyle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class advertisement_detail extends AppCompatActivity {

    TextView textTitle, textDate, textTime, textDescription;

    ImageView adPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_advertisement_detail);

        textTitle = findViewById(R.id.textTitle);
        textDate = findViewById(R.id.textDate);
        textTime = findViewById(R.id.textTime);
        textDescription = findViewById(R.id.textDescription);

        adPicture = findViewById(R.id.adPicture);

        String title = getIntent().getStringExtra("title");
        String time = getIntent().getStringExtra("time");
        String date = getIntent().getStringExtra("date");
        String description = getIntent().getStringExtra("description");

        String url = getIntent().getStringExtra("url");

        textTitle.setText(title);
        textTime.setText(time);
        textDate.setText(date);
        textDescription.setText(description);

        Picasso.get().load(url).into(adPicture);
    }
}