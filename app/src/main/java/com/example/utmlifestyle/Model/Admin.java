package com.example.utmlifestyle.Model;

public class Admin {

    public String name, id, dob, phone, email, address;

    public Admin() {
    }

    public Admin(String name, String id, String dob, String phone, String email, String address) {
        this.name = name;
        this.id = id;
        this.dob = dob;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }
}
