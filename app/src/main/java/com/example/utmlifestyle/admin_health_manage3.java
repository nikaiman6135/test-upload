package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class admin_health_manage3 extends AppCompatActivity {

    TextView userID, date, reason;
    EditText note, medication;

    String gID, gDate, gReason, gMed, gNote;

    Button delete, update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_health_manage3);

        userID = findViewById(R.id.userID);
        date = findViewById(R.id.date);
        reason = findViewById(R.id.reason);
        note = findViewById(R.id.note);
        medication = findViewById(R.id.medication);

        delete = findViewById(R.id.delete);
        update = findViewById(R.id.update);

        Intent intent = getIntent();
        gID = intent.getStringExtra("userID");
        gDate = intent.getStringExtra("date");
        gReason = intent.getStringExtra("reason");
        gNote = intent.getStringExtra("note");
        gMed = intent.getStringExtra("medication");

        userID.setText(gID);
        date.setText(gDate);
        reason.setText(gReason);
        note.setText(gNote);
        medication.setText(gMed);

        String cID = userID.getText().toString().trim()+"+"+date.getText().toString().trim();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
                Query query = reference.child("Health_Record").orderByChild("cID").equalTo(cID);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot snapshot1: snapshot.getChildren()){
                            snapshot1.getRef().removeValue();

                            Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Toast.makeText(getApplicationContext(), "Connection to database failed.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase.getInstance().getReference().child("Health_Record").child(cID).child("note").setValue(note.getText().toString().trim());
                FirebaseDatabase.getInstance().getReference().child("Health_Record").child(cID).child("medication").setValue(medication.getText().toString().trim());


                Toast.makeText(getApplicationContext(), "Success.", Toast.LENGTH_SHORT).show();
            }
        });


    }
}