package com.example.utmlifestyle;

public class VenueSearch {

    private String date, time, venue, payment, id, bookingID;

    public VenueSearch() {
    }

    public VenueSearch(String date, String time, String venue, String payment, String id, String bookingID) {
        this.date = date;
        this.time = time;
        this.venue = venue;
        this.payment = payment;
        this.id = id;
        this.bookingID = bookingID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getVenue() {
        return venue;
    }

    public void setVenue(String venue) {
        this.venue = venue;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookingID() {
        return bookingID;
    }

    public void setBookingID(String bookingID) {
        this.bookingID = bookingID;
    }
}
