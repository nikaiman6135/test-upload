package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class venue_booking extends AppCompatActivity {

    //spinner edit text
    EditText payment, time, date, venue;

    //David's Spinner
    Spinner spinner_sport_hall, spinner_date, spinner_time;
    Button button_confirm;
    DatabaseReference reference;
    List<Venue_booking_statuss> venue_booking_statusList = new ArrayList<>();
    List<String> sport_hall = new ArrayList<>();
    List<String> avaiable_date = new ArrayList<>();
    List<String> avaiable_time = new ArrayList<>();
    ArrayAdapter<String> adapter;
    ArrayAdapter<String> adapterdate;
    ArrayAdapter<String> adaptertime;
    String text_spinnerplace, text_spinnerdate, text_spinnertime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_booking);

        payment =  findViewById(R.id.payment);
        venue =  findViewById(R.id.sportsVenue);
        time =  findViewById(R.id.etChooseTime);
        date =  findViewById(R.id.date);


        //get id number
        String userID;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Users");
        final TextView id = findViewById(R.id.id);
        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);
                if (userProfile != null){
                    String ids = userProfile.id;
                    id.setText(ids);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(venue_booking.this, "Something wrong happened!", Toast.LENGTH_LONG).show();
            }
        });
        //end of get id number

        //spinner venue, time, date
        spinner_sport_hall = findViewById(R.id.spinner_sport_hall);
        spinner_date = findViewById(R.id.spinner_date);
        spinner_time = findViewById(R.id.spinner_time);
        button_confirm = findViewById(R.id.submitButton);

        reference = FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                sport_hall.clear();
                venue_booking_statusList.clear();
                for(DataSnapshot snapshot1 : snapshot.getChildren()) {
                    Venue_booking_statuss venue_booking_status1 = snapshot1.getValue(Venue_booking_statuss.class);
                    venue_booking_statusList.add(venue_booking_status1);

                    if(sport_hall.size()==0){
                        sport_hall.add(venue_booking_status1.getName());
                    }
                    else{
                        for(int i = 0; i<sport_hall.size();i++){
                            if(venue_booking_statusList.get(i).getName().equals(venue_booking_status1.getName())){

                            }
                            else{
                                sport_hall.add(venue_booking_status1.getName());
                            }
                        }
                    }
                }

                adapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner1, sport_hall);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_sport_hall.setAdapter(adapter);
                spinner_sport_hall.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        text_spinnerplace = parent.getItemAtPosition(position).toString();
                        venue.setText(text_spinnerplace);
                        avaiable_date.clear();
                        for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                            if(avaiable_date.size()==0){
                                avaiable_date.add(venue_booking_statusList.get(i).getDate());
                            }
                            else{

                                if(venue_booking_statusList.get(i).getDate().equals(avaiable_date.get(avaiable_date.size()-1))){

                                }
                                else{
                                    avaiable_date.add(venue_booking_statusList.get(i).getDate());
                                }
                            }
                        }
                        adapterdate = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner1, avaiable_date);
                        adapterdate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_date.setAdapter(adapterdate);
                        spinner_date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                text_spinnerdate = parent.getItemAtPosition(position).toString();
                                date.setText(text_spinnerdate);
                                //Toast.makeText(parent.getContext(),text,Toast.LENGTH_SHORT).show();
                                avaiable_time.clear();
                                for(int i = 0 ; i<venue_booking_statusList.size() ; i++){
                                    if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getRent_status().equals("a")){
                                        avaiable_time.add(venue_booking_statusList.get(i).getTime());
                                    }
                                }

                                adaptertime = new ArrayAdapter<String>(getApplicationContext(), R.layout.custom_spinner1, avaiable_time);
                                adaptertime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spinner_time.setAdapter(adaptertime);
                                spinner_time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        text_spinnertime = parent.getItemAtPosition(position).toString();
                                        time.setText(text_spinnertime);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> adapterView) {

                            }
                        });
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //end of spinner

        //payment method spinner
        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.venuePayment, R.layout.custom_spinner1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getItemAtPosition(position).toString();
                payment.setText(text);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
        //end of payment method spinner

        EditText mid = findViewById(R.id.id);
        EditText mpayment =  findViewById(R.id.payment);

        //insert into database
        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] timesplit = text_spinnertime.split("");
                String timeshort = timesplit[0]+timesplit[1];
                String child = text_spinnerplace+";"+text_spinnerdate+";"+timeshort;
                String bookingID = "VBKG"+"-"+text_spinnerplace+"-"+text_spinnerdate+"-"+timeshort;
                String bookingIDD = bookingID.replaceAll(" ","");
                //Toast.makeText(venue_booking_form.this,child,Toast.LENGTH_SHORT).show();
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(child).child("pay_method").setValue(mpayment.getText().toString().trim());
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(child).child("rent_id").setValue(bookingIDD);
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(child).child("rent_person_name").setValue(mid.getText().toString().trim());
                FirebaseDatabase.getInstance().getReference().child("Venue_booking_statuss").child(child).child("rent_status").setValue("n");

                String detail = "";
                for(int i = 0 ; i < venue_booking_statusList.size(); i++){
                    if(venue_booking_statusList.get(i).getName().equals(text_spinnerplace)&&venue_booking_statusList.get(i).getDate().equals(text_spinnerdate)&&venue_booking_statusList.get(i).getTime().equals(text_spinnertime)){
                        detail = venue_booking_statusList.get(i).getVenue_detail();
                        break;
                    }
                }

                Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), venue_confirm.class);
                intent.putExtra("venue", text_spinnerplace);
                intent.putExtra("date", text_spinnerdate);
                intent.putExtra("time", text_spinnertime);
                intent.putExtra("bookingID", bookingIDD);
                startActivity(intent);
                finish();
            }
        });
        //end of insert into database
    }
}