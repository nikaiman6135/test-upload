package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class venue_home extends AppCompatActivity {

    private Button newBooking, searchDateBtn;
    EditText mdate;
    EditText edittext;
    String gDate;
    TextView history;

    RecyclerView recyclerView_booking_list;
    Booking_List_Adapter booking_list_adapter;
    List<Venue_booking_statuss> booking_listList = new ArrayList<>();
    DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue_home);

        recyclerView_booking_list = findViewById(R.id.recyclerview_booking_list);
        recyclerView_booking_list.setHasFixedSize(true);
        recyclerView_booking_list.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        //get id number
        String userID;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        DatabaseReference reference2 = FirebaseDatabase.getInstance().getReference("Users");
        final TextView id = findViewById(R.id.id);
        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);
                if (userProfile != null){
                    String ids = userProfile.id;
                    id.setText(ids);

                    //recyclerview
                    reference  = FirebaseDatabase.getInstance().getReference("Venue_booking_statuss");
                    Query query = reference.orderByChild("rent_person_name").equalTo(ids);
                    query.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            booking_listList.clear();
                            for(DataSnapshot snapshot1 : snapshot.getChildren()){
                                Venue_booking_statuss venue_booking_statuss = snapshot1.getValue(Venue_booking_statuss.class);
                                if(venue_booking_statuss.getRent_status().equals("n")){
                                    booking_listList.add(venue_booking_statuss);
                                }
                            }
                            booking_list_adapter = new Booking_List_Adapter(getApplicationContext(), booking_listList);
                            booking_list_adapter.setOnItemClickListener(new Booking_List_Adapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(getApplicationContext(), venue_manage.class);
                                    intent.putExtra("date", booking_listList.get(position).getDate());
                                    intent.putExtra("time", booking_listList.get(position).getTime());
                                    intent.putExtra("venue", booking_listList.get(position).getName());
                                    intent.putExtra("payment", booking_listList.get(position).getPay_method());
                                    intent.putExtra("id", booking_listList.get(position).getRent_person_name());
                                    intent.putExtra("bookingID", booking_listList.get(position).getRent_id());
                                    startActivity(intent);
                                }
                            });
                            recyclerView_booking_list.setAdapter(booking_list_adapter);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {

                        }
                    });
                    //end of recyclerview
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getApplicationContext(), "Something wrong happened!", Toast.LENGTH_LONG).show();
            }
        });
        //end of get id number


        //date picker
        mdate = findViewById(R.id.date);
        final Calendar myCalendar = Calendar.getInstance();
        edittext = (EditText) findViewById(R.id.date);
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

            private void updateLabel() {
                String myFormat = "dd MMM yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                edittext.setText(sdf.format(myCalendar.getTime()));
                mdate.setText(sdf.format(myCalendar.getTime()));
                gDate = mdate.getText().toString().trim();
            }
        };

        edittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(venue_home.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        //end of date picker


        newBooking = findViewById(R.id.newBooking);
        newBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), venue_booking.class);
                startActivity(intent);
            }
        });

        history = findViewById(R.id.history);
        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), venue_history.class);
                startActivity(i);
            }
        });

        searchDateBtn = findViewById(R.id.searchDateBtn);
        searchDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(mdate.getText())){
                    Toast.makeText(venue_home.this, "Please enter a date!", Toast.LENGTH_LONG).show();
                }else {

                    //Toast.makeText(clinic_home.this, gDate, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getApplicationContext(), venue_search.class);
                    intent.putExtra("date", gDate);
                    startActivity(intent);
                }
            }
        });

    }
}