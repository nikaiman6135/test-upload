package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import org.w3c.dom.Text;

public class admin_health_manage2 extends AppCompatActivity {

    private FirebaseRecyclerOptions<HRecordSearch> display;
    private FirebaseRecyclerAdapter<HRecordSearch, MyViewHolder2> adapter;

    private RecyclerView recyclerView;

    DatabaseReference ref;
    Query query;

    String gDate;

    TextView vDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_health_manage2);

        Intent intent = getIntent();
        gDate = intent.getStringExtra("date");
        vDate = findViewById(R.id.date);
        vDate.setText(gDate);

        ref = FirebaseDatabase.getInstance().getReference().child("Health_Record");
        query = ref.orderByChild("date").equalTo(gDate);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        display = new FirebaseRecyclerOptions.Builder<HRecordSearch>().setQuery(query, HRecordSearch.class).build();
        adapter = new FirebaseRecyclerAdapter<HRecordSearch, MyViewHolder2>(display) {
            @Override
            protected void onBindViewHolder(@NonNull MyViewHolder2 holder, int position, @NonNull HRecordSearch model) {
                holder.userID.setText("" + model.getId());
                holder.date.setText("" + model.getDate());
                holder.medication.setText("" + model.getMedication());
                holder.reason.setText("" + model.getReason());
                holder.note.setText("" + model.getNote());

                holder.view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), admin_health_manage3.class);
                        intent.putExtra("userID", model.getId());
                        intent.putExtra("date", model.getDate());
                        intent.putExtra("medication", model.getMedication());
                        intent.putExtra("reason", model.getReason());
                        intent.putExtra("note", model.getNote());
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public MyViewHolder2 onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.health_record1, parent, false);
                return new MyViewHolder2(v);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);

    }
}