package com.example.utmlifestyle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Pattern;

public class contact_form extends AppCompatActivity {

    EditText emailInput, subjectInput, messageInput, phoneInput;

    private EditText ETemail, ETphone;


    Button submitButton;
    DatabaseReference reference, reference2;

    Contact contact;

    private FirebaseUser user;
    private String userID;

    private boolean validEmail(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean validPhone(String phone) {
        Pattern pattern = Patterns.PHONE;
        return pattern.matcher(phone).matches();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_form);

        emailInput = findViewById(R.id.emailInput);
        subjectInput = findViewById(R.id.subjectInput);
        messageInput = findViewById(R.id.messageInput);
        phoneInput = findViewById(R.id.phoneInput);
        submitButton = findViewById(R.id.submitButton);

        user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        reference2 = FirebaseDatabase.getInstance().getReference("Users");

        ETemail = findViewById(R.id.emailInput);
        ETphone = findViewById(R.id.phoneInput);

        final TextView emailInput = findViewById(R.id.emailInput);
        final TextView phoneInput = findViewById(R.id.phoneInput);

        reference2.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);

                if (userProfile != null){
                    String email = userProfile.email;
                    String phone = userProfile.phone;


                    emailInput.setText(email);
                    phoneInput.setText(phone);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

                Toast.makeText(contact_form.this, "Something wrong happened!", Toast.LENGTH_LONG).show();

            }
        });

        contact = new Contact();
        reference = FirebaseDatabase.getInstance().getReference().child("Contact");

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int phone2;
                try {
                    phone2 = Integer.parseInt(phoneInput.getText().toString());
                }
                catch(NumberFormatException ex) {
                    Toast.makeText(contact_form.this, "All Fields Are Required", Toast.LENGTH_LONG).show();
                    return;
                }
                contact.setEmail(emailInput.getText().toString());
                contact.setSubject(subjectInput.getText().toString());
                contact.setMessage(messageInput.getText().toString());
                contact.setPhone(phone2);

                if(TextUtils.isEmpty(phoneInput.getText())){

                    Toast.makeText(contact_form.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    phoneInput.setError( "Dont Leave Me Empty :(" );

                }else if(!validPhone(phoneInput.getText().toString())){

                    phoneInput.setError( "Invalid Phone Number" );

                }else if(TextUtils.isEmpty(emailInput.getText())){

                    Toast.makeText(contact_form.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    emailInput.setError( "Dont Leave Me Empty :(" );

                }else if(!validEmail(emailInput.getText().toString())){

                    emailInput.setError( "Invalid Email address" );

                }else if(TextUtils.isEmpty(subjectInput.getText())){

                    Toast.makeText(contact_form.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    subjectInput.setError( "Dont Leave Me Empty :(" );

                }else if(TextUtils.isEmpty(messageInput.getText())){

                    Toast.makeText(contact_form.this, "All Fields Are Required", Toast.LENGTH_LONG).show();

                    messageInput.setError( "Dont Leave Me Empty :(" );

                }else{
                    reference.push().setValue(contact);

                    Intent i = new Intent(getApplicationContext(), contact_redirect.class);
                    startActivity(i);
                    finish();
                }

            }
        });

        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}

    }
}